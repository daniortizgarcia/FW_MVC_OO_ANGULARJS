<?php

	class controller_ubication{
		function __construct(){
			$_SESSION['module'] = "ubication";
		}
		function load_location(){

			$json = loadModel(MODEL_UBICATION, "ubication_model", "load_location");
			echo json_encode($json);
		}
		function load_prov(){
			$json = loadModel(MODEL_UBICATION, "ubication_model", "load_prov");
			echo json_encode($json);
		}

		function get_lat_lng(){
			$prov = $_GET['param'];
			$ubi = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($prov).'&key=AIzaSyARtCW9CopqKbhJ5imTUBqmyqMpZss2AZQ');
			$ubi = json_decode($ubi, true);
			if ($ubi['status'] = 'OK') {
				$latitud = $ubi['results'][0]['geometry']['location']['lat'];
				$longitud = $ubi['results'][0]['geometry']['location']['lng'];
				$point = '[{"lat":"' . $latitud . '","long":"' . $longitud . '"}]';
			}
			echo $point;
		}
	}