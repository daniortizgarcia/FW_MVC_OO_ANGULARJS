<?php
    class controller_dogs {
        function __construct() {
            include(UTILS_DOGS . "functions_dogs.inc.php");
            $_SESSION['module'] = "dogs";
        }

        function json_dog(){
              alta_dogs();
        }
        function upload_dog(){
          $result_dogpic = upload_files();
          $_SESSION['result_dogpic'] = $result_dogpic;
          echo json_encode($result_dogpic);
        }

        function delete_dog(){
          $_SESSION['result_dogpic'] = array();
          $result = remove_files();
          if($result === true){
            echo json_encode(array("res" => true));
          }else{
            echo json_encode(array("res" => false));
          }
        }

        function load_provinces(){
              $jsondata = array();
              $json = array();

            $json = loadModel(MODEL_DOGS, "dogs_model", "obtain_provinces");

            if($json){
              $jsondata["provinces"] = $json;
              echo json_encode($jsondata);
              exit;
            }else{
              $jsondata["provinces"] = "error";
              echo json_encode($jsondata);
              exit;
            }
        }

        function load_cities(){
            $jsondata = array();
            $json = array();

            $json = loadModel(MODEL_DOGS, "dogs_model", "obtain_cities",$_GET['param']);

            if($json){
              $jsondata["cities"] = $json;
              echo json_encode($jsondata);
              exit;
            }else{
              $jsondata["cities"] = "error";
              echo json_encode($jsondata);
              exit;
            }
        }
    }

function alta_dogs(){
	$jsondata = array();
	$result=validate_dogs(json_decode($_POST['json_dog'], true));
  $token = json_decode($_POST['json_dog'],true);
  $id_user = loadModel(MODEL_DOGS, "dogs_model", "select_creator", $token['token']);
  
    if (empty($_SESSION['result_dogpic'])){
      $_SESSION['result_dogpic'] = array('result' => true, 'error' => "", "data" => "/workspace/miejer/FW_MVC_OO_JS_ANGULARJS/backend/media/default_avatar.svg");
      $result_dogpic = $_SESSION['result_dogpic'];
    }else{
      $result_dogpic = $_SESSION['result_dogpic'];
      $result_dogpic['data'] = substr($_SESSION['result_dogpic']['data'],17);
    }
    
	if($result['result'] && $result_dogpic['result']) {

		$arrArgument = array(
      'dname' => $result['data']['dname'],
      'dchip' => $result['data']['dchip'],
      'breed' => $result['data']['breed'],
      'dtlp' => $result['data']['dtlp'],
      'sex' => $result['data']['sex'],
      'stature' => $result['data']['stature'],
      'date_birth' => $result['data']['date_birth'],
      'province' => $result['data']['province'],
      'city' => $result['data']['city'],
      'dinfo' => $result['data']['dinfo'],
      'dogpic' => $result_dogpic['data'],
      'id_user' =>$id_user[0]['IDuser']
    );

    $arrValue = false;
    $arrValue = loadModel(MODEL_DOGS, "dogs_model", "create_dog", $arrArgument);

    if ($arrValue){
        $message = "El perro ha sido registrado correctamente";
    }else{
        $message = "El perro no se ha podido registar en base de datos";
    }

		$jsondata['dogs'] = $arrArgument;
    $_SESSION['message'] = $message;
  	$jsondata['success'] = true;
  	$jsondata['redirect']="../../dogs/result_dogs/";

    unset($_SESSION['result_dogpic']);
    echo json_encode($jsondata);
   	exit();
 	}else{
 		$jsondata['success'] = false;
 		$jsondata['error'] = $result['error'];
    $jsondata['error_pic'] = $result_dogpic['error'];
    echo json_encode($jsondata);
 	}
}