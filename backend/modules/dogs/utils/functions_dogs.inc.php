<?php
	function validate_dogs($data){
		
		$error = array();
	    $valid = true;
	    $filter = array(
	        'dname' => array(
	            'filter' => FILTER_VALIDATE_REGEXP,
	            'options' => array('regexp' => '/^[A-Za-z]{2,21}$/')
	        ),
	        'dchip' => array(
	            'filter' => FILTER_VALIDATE_REGEXP,
	            'options' => array('regexp' => '/^[0-9]{6}[A-Z]{1}$/')
	        ),
	        'breed' => array(
	            'filter' => FILTER_VALIDATE_REGEXP,
	            'options' => array('regexp' => '/^[A-Za-z\s]{2,21}$/')
	        ),
	        'dtlp' => array(
	            'filter' => FILTER_VALIDATE_REGEXP,
	            'options' => array('regexp' => '/^[0-9]{9}$/')
	        ),
	        'date_birth' => array(
	            'filter' => FILTER_VALIDATE_REGEXP,
	            'options' => array('regexp' => '/^([0-9]{1,2})\\/([0-9]{1,2})\\/([0-9]{4})$/')
	        ),
	        'dinfo' => array(
	            'filter' => FILTER_VALIDATE_REGEXP,
	            'options' => array('regexp' => '/^[0-9A-Za-z\s]{20,100}$/')
	        ),
	    );

    	$result = filter_var_array($data, $filter);

    	$result['sex'] = $data['sex'];
    	$result['stature'] = $data['stature'];
	    $result['province'] = $data['province'];
	    $result['city'] = $data['city'];

    	if ($result != null && $result){
	        if(validate_chip($result['dchip'])){
	            $error['dchip'] = "El chip ya esta registrado en el base de datos";
	            $valid = false;
	        }
	        if(validate_birth($data['date_birth'])){
	            $error['date_birth'] = "El perro tiene que tener mas de mes y medio de edad.";
	            $valid = false;
	        }
	    } else {
	        $valid = false;
	    };

		return $return = array('result' => $valid,'error' => $error, 'data' => $result);
	}


	function validate_birth($date){

		$thisdate = getdate();
		$resultado = strtotime($thisdate['mon'] . "/" . $thisdate['mday'] . "/" . $thisdate['year']) - strtotime($date);
		$oper=$resultado/(60*60*24);
		if($oper < 45){
			return  true;
		} else{
			return false;
		}

	}

	function validate_chip($chip){
		$val = loadModel(MODEL_DOGS,"dogs_model","nduplicate_chip",$chip);
		return $val;	
	}