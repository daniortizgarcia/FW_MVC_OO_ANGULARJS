<?php

	class adoptions_bll{
	    private $dao;
	    private $db;
	    static $_instance;

	    private function __construct() {
	        $this->dao = adoptions_dao::getInstance();
	        $this->db = db::getInstance();
	    }
	    public static function getInstance() {
	        if (!(self::$_instance instanceof self)){
	            self::$_instance = new self();
	        }
	        return self::$_instance;
	    }
	    public function obtain_data_list_BLL($arrArgument){
	      return $this->dao->select_data_list($this->db,$arrArgument);
	    }

	    public function obtain_data_details_BLL($arrArgument){
	      return $this->dao->select_data_details($this->db,$arrArgument);
	    }
	    public function all_breeds_BLL(){
	      return $this->dao->select_all_breeds($this->db);
	    }
	    public function select_user_BLL($arrArgument){
	      $arrArgument2 = $this->dao->select_user($this->db,$arrArgument['token']);
	      $this->dao->insert_adoption($this->db,$arrArgument2[0]['IDuser'],$arrArgument["chip"]);
	       return $this->dao->update_value($this->db,$arrArgument["chip"]);
	    }
	}