<?php

class adoptions_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = adoptions_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function obtain_data_list($arrArgument){
        return $this->bll->obtain_data_list_BLL($arrArgument);
    }
    public function obtain_data_details($arrArgument){
        return $this->bll->obtain_data_details_BLL($arrArgument);
    }
    public function all_breeds(){
        return $this->bll->all_breeds_BLL();
    }
    public function select_user($arrArgument){
        return $this->bll->select_user_BLL($arrArgument);
    }
}