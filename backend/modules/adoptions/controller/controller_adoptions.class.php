<?php

	class controller_adoptions {
		
		function __construct(){
	        $_SESSION['module'] = "adoptions";
		}
		function all_breeds(){
			$json = array();
		 	$json = loadModel(MODEL_ADOPTIONS, "adoptions_model", "all_breeds");
		 	echo json_encode($json);
		}
		function load_list(){
			$json = array();
		 	$json = loadModel(MODEL_ADOPTIONS, "adoptions_model", "obtain_data_list",$_GET['param']);

		 	echo json_encode($json);
		}
		function details_list(){
				$json = array();
			 	$json = loadModel(MODEL_ADOPTIONS, "adoptions_model", "obtain_data_details",$_GET['param']);

			 	echo json_encode($json);
		}
		function adoption_dog(){
			$info = json_decode($_POST["all_info"],true);
			$json = loadModel(MODEL_ADOPTIONS, "adoptions_model", "select_user",$info);
			echo json_encode($json);
		}
	}