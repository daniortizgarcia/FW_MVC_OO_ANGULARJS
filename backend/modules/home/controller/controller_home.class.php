<?php
	class controller_home {
	    function __construct() {
	        $_SESSION['module'] = "home";
	    }

	    function best_breed() {
				$json = array();
			 	$json = loadModel(MODEL_HOME, "home_model", "best_breed_home");

			 	echo json_encode($json);
	    }

	    function load_name(){
				$json = array();
			 	$json = loadModel(MODEL_HOME, "home_model", "load_name");
			 	echo json_encode($json);
	    }

	    function select_auto_name() {
				$json = array();
			 	$json = loadModel(MODEL_HOME, "home_model", "select_auto_name",$_GET['param']);

			 	echo json_encode($json);
	    }

	    function load_list(){
				$json = array();
			 	$json = loadModel(MODEL_HOME, "home_model", "obtain_data_list",$_GET["param"]);

			 	echo json_encode($json);
	    }
	    function details_list(){
			$json = array();
		 	$json = loadModel(MODEL_HOME, "home_model", "obtain_data_details",$_GET['param']);

		 	echo json_encode($json);
	    }

	    function active_user(){
	    	$token = json_decode($_POST['token'],true);
    		loadModel(MODEL_HOME, "home_model", "active_user",$token['token']);
	    }

	}