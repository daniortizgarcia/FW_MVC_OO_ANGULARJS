<?php
    class controller_blog {
        function __construct() {
            $_SESSION['module'] = "blog";
        }

        function insert_blog(){
          $data_blog = json_decode($_POST['data_blog'],true);
          if (isset($_SESSION['result_dogpic']) && $_SESSION['result_dogpic']['result']) {
            $data_blog['cover'] = $_SESSION['result_dogpic']['data'];
            $json = loadModel(MODEL_BLOG, "blog_model", "insert_blog",$data_blog);
            echo json_encode($json);  
          }else{
            echo "error";
          }
          
        }

        function load_blogs(){
          $json = loadModel(MODEL_BLOG, "blog_model", "load_blogs");
          echo json_encode($json);
        }
        function load_comments(){
          $json = loadModel(MODEL_BLOG, "blog_model", "load_comments");
          echo json_encode($json);
        }

        function upload_cover(){
          $result_dogpic = upload_files();
          $_SESSION['result_dogpic'] = $result_dogpic;
          echo json_encode($result_dogpic);
        }

        function delete_cover(){
          $_SESSION['result_dogpic'] = array();
          $result = remove_files();
          if($result === true){
            echo json_encode(array("res" => true));
          }else{
            echo json_encode(array("res" => false));
          }
        }
        function stouch(){
          $json = loadModel(MODEL_BLOG, "blog_model", "stouch",$_POST['id']);
          echo json_encode($json);
        }
        function insert_comment(){
          $json = loadModel(MODEL_BLOG, "blog_model", "insert_comment",json_decode($_POST['data_comment'],true));
          echo json_encode($json);
        }
        function get_user(){
          $json = loadModel(MODEL_BLOG, "blog_model", "get_user",$_GET['param']);
          echo json_encode($json[0]);
        }
        function insert_val(){
          $json = loadModel(MODEL_BLOG, "blog_model", "insert_val", json_decode($_POST["infoVal"],true));
          echo json_encode($json);
        }
        function load_rate(){
          $json = loadModel(MODEL_BLOG, "blog_model", "load_rate");
          echo json_encode($json);
        }
    }