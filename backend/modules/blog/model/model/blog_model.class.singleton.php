<?php

class blog_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = blog_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
	
    public function insert_blog($arrArgument) {
        return $this->bll->insert_blog_BLL($arrArgument);
    }

    public function load_blogs() {
        return $this->bll->load_blogs_BLL();
    }
    public function load_comments() {
        return $this->bll->load_comments_BLL();
    }
    public function stouch($arrArgument) {
        return $this->bll->stouch_BLL($arrArgument);
    }
    public function insert_comment($arrArgument) {
        return $this->bll->insert_comment_BLL($arrArgument);
    }
    public function get_user($arrArgument) {
        return $this->bll->get_user_BLL($arrArgument);
    }
    public function insert_val($arrArgument) {
        return $this->bll->insert_val_BLL($arrArgument);
    }
    public function load_rate() {
        return $this->bll->load_rate_BLL();
    }
}