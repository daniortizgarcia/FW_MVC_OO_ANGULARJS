<?php
class blog_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function insert_blog($db,$arrArgument) {
        $id = md5(uniqid(rand(),true));
        $title = $arrArgument['title'];
        $text = $arrArgument['text'];
        $cover = substr($arrArgument['cover'],17);
        $IDblogger = $arrArgument['IDblogger'];
        $blogger = $arrArgument['blogger'];
        
        $sql = "INSERT INTO blog (id, title, text, cover,touched,IDblogger,blogger, data) 
                VALUES ('$id','$title','$text','$cover',0,'$IDblogger','$blogger',now())";

        return $db->ejecutar($sql);
    }
    public function select_user($db,$arrArgument) {
        $token = $arrArgument['token'];
        $sql = "SELECT IDuser,name,surname FROM users WHERE tokenlog = '$token'";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function load_blogs($db) {
        
        $sql = "SELECT * FROM blog";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function load_comments($db) {
        
        $sql = "SELECT * FROM comments ORDER BY dateb";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function stouch($db,$arrArgument) {
        $sql = "UPDATE blog SET touched = touched + 1 WHERE id = '$arrArgument'";

        return $db->ejecutar($sql);
    }

    public function insert_comment($db,$arrArgument) {
        $id = md5(uniqid(rand(),true));
        $idblog = $arrArgument['idblog'];
        $text = $arrArgument['text'];
        $IDuser = $arrArgument['IDuser'];
        
        $sql = "INSERT INTO comments (IDcomment, IDblog, text, IDuser, dateb) 
                VALUES ('$id','$idblog','$text','$IDuser',now())";

        return $db->ejecutar($sql);
    }

    public function get_user($db,$arrArgument) {
        
        $sql = "SELECT IDuser,user,email,avatar,name,surname,birthday FROM users WHERE IDuser = '$arrArgument'";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function select_user_val($db,$arrArgument) {
        $user = $arrArgument['IDuser'];
        $blog = $arrArgument['idblog'];
        $sql = "SELECT * FROM rate WHERE IDuser = '$user' AND idblog = '$blog'";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function insert_val($db,$arrArgument) {
        $token = md5(uniqid(rand(),true));;
        $user = $arrArgument['IDuser'];
        $blog = $arrArgument['idblog'];
        $numb = $arrArgument['numb'];
        $sql = "INSERT INTO rate(IDrate, IDblog, IDuser, titration) VALUES('$token','$blog','$user','$numb') ";

        return $db->ejecutar($sql);
    }

    public function load_rate($db) {
        
        $sql = "SELECT IDblog,(SUM(titration) / COUNT(*)) suma, COUNT(*) votos FROM `rate` WHERE 1 GROUP BY IDblog";

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }
}//End productDAO
