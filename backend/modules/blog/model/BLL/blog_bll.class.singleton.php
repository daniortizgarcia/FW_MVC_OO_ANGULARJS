<?php
class blog_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = blog_dao::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function insert_blog_BLL($arrArgument){
      $creator = $this->dao->select_user($this->db,$arrArgument);
      $arrArgument['IDblogger'] = $creator[0]['IDuser'];
      $arrArgument['blogger'] = $creator[0]['name'] . ' ' . $creator[0]['surname'];
      return $this->dao->insert_blog($this->db,$arrArgument);
    }

    public function load_blogs_BLL(){
      return $this->dao->load_blogs($this->db);
    }
    public function load_comments_BLL(){
      return $this->dao->load_comments($this->db);
    }
    public function stouch_BLL($arrArgument){
      return $this->dao->stouch($this->db,$arrArgument);
    }
    public function insert_comment_BLL($arrArgument){
      $user = $this->dao->select_user($this->db,$arrArgument);
      $arrArgument['IDuser'] = $user[0]['IDuser'];
      return $this->dao->insert_comment($this->db,$arrArgument);
    }
    public function get_user_BLL($arrArgument){
      return $this->dao->get_user($this->db,$arrArgument);
    }
    public function insert_val_BLL($arrArgument){
      $user = $this->dao->select_user($this->db,$arrArgument);
      $arrArgument['IDuser'] = $user[0]['IDuser'];
      $usval = $this->dao->select_user_val($this->db,$arrArgument);
      if (count($usval) > 0) {
        return false;
      }else{
        return $this->dao->insert_val($this->db,$arrArgument);
      }
      
    }
    public function load_rate_BLL(){
      return $this->dao->load_rate($this->db);
    }
}
