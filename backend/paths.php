<?php

//SITE_ROOT
$path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/miejer/FW_MVC_OO_JS_ANGULARJS/backend/';
define('SITE_ROOT', $path);

//SITE_PATH
define('SITE_PATH', 'http://' . $_SERVER['HTTP_HOST'] . '/workspace/miejer/FW_MVC_OO_JS_ANGULARJS/backend/');

//PRODUCTION
define('PRODUCTION', true);

//MODEL
define('MODEL_PATH', SITE_ROOT . 'model/');

//MODULES
define('MODULES_PATH', SITE_ROOT . 'modules/');

//RESOURCES
define('RESOURCES', SITE_ROOT . 'resources/');

//MEDIA
define('MEDIA_PATH',SITE_ROOT . 'media/');

//UTILS
define('UTILS', SITE_ROOT . 'utils/');

//LIBS
define('LIBS', SITE_ROOT . 'libs/');

//CLASSES
define('CLASSES', SITE_ROOT . 'classes/');

//MODEL_HOME
define('UTILS_HOME', SITE_ROOT . 'modules/home/utils/');
define('MODEL_PATH_HOME', SITE_ROOT . 'modules/home/model/');
define('DAO_HOME', SITE_ROOT . 'modules/home/model/DAO/');
define('BLL_HOME', SITE_ROOT . 'modules/home/model/BLL/');
define('MODEL_HOME', SITE_ROOT . 'modules/home/model/model/');

//MODEL_DOGS
define('UTILS_DOGS', SITE_ROOT . 'modules/dogs/utils/');
define('MODEL_PATH_DOGS', SITE_ROOT . 'modules/dogs/model/');
define('DAO_DOGS', SITE_ROOT . 'modules/dogs/model/DAO/');
define('BLL_DOGS', SITE_ROOT . 'modules/dogs/model/BLL/');
define('MODEL_DOGS', SITE_ROOT . 'modules/dogs/model/model/');

//MODEL_ADOPTIONS
define('UTILS_ADOPTIONS', SITE_ROOT . 'modules/adoptions/utils/');
define('MODEL_PATH_ADOPTIONS', SITE_ROOT . 'modules/adoptions/model/');
define('DAO_ADOPTIONS', SITE_ROOT . 'modules/adoptions/model/DAO/');
define('BLL_ADOPTIONS', SITE_ROOT . 'modules/adoptions/model/BLL/');
define('MODEL_ADOPTIONS', SITE_ROOT . 'modules/adoptions/model/model/');

//MODEL_UBICATION
define('UTILS_UBICATION', SITE_ROOT . 'modules/ubication/utils/');
define('MODEL_PATH_UBICATION', SITE_ROOT . 'modules/ubication/model/');
define('DAO_UBICATION', SITE_ROOT . 'modules/ubication/model/DAO/');
define('BLL_UBICATION', SITE_ROOT . 'modules/ubication/model/BLL/');
define('MODEL_UBICATION', SITE_ROOT . 'modules/ubication/model/model/');

//MODEL_LOGIN
define('UTILS_LOGIN', SITE_ROOT . 'modules/login/utils/');
define('MODEL_PATH_LOGIN', SITE_ROOT . 'modules/login/model/');
define('DAO_LOGIN', SITE_ROOT . 'modules/login/model/DAO/');
define('BLL_LOGIN', SITE_ROOT . 'modules/login/model/BLL/');
define('MODEL_LOGIN', SITE_ROOT . 'modules/login/model/model/');

//MODEL_BLOG
define('UTILS_BLOG', SITE_ROOT . 'modules/blog/utils/');
define('MODEL_PATH_BLOG', SITE_ROOT . 'modules/blog/model/');
define('DAO_BLOG', SITE_ROOT . 'modules/blog/model/DAO/');
define('BLL_BLOG', SITE_ROOT . 'modules/blog/model/BLL/');
define('MODEL_BLOG', SITE_ROOT . 'modules/blog/model/model/');

//amigables
define('URL_AMIGABLES', TRUE);