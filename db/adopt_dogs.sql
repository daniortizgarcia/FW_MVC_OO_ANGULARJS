-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 11-06-2018 a las 17:17:32
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `adopt_dogs`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adoption`
--

CREATE TABLE `adoption` (
  `id_token` varchar(105) NOT NULL,
  `user` varchar(50) NOT NULL,
  `dog` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adoption`
--

INSERT INTO `adoption` (`id_token`, `user`, `dog`) VALUES
('215278d8c25d48a524568af221349b73', 'dani', '917345B'),
('3341bf8bea4ecf94bb18d88d4c5d2ecb', 'dani', '736123E'),
('37c36f05568b1c7122664fbc8f200b24', 'YerBwm1SxtNCPvew4XlomKmYWEb2', '736123E'),
('38b6c3982703d0626022da7845b36c8f', 'YerBwm1SxtNCPvew4XlomKmYWEb2', '542312F'),
('4d1b3de138dbf02a5b3818eb1e965951', 'dani', '735234S'),
('5f5633a8f45a6cfa421fb35006e735b6', 'YerBwm1SxtNCPvew4XlomKmYWEb2', '521123G'),
('60d0687d06b53d169bf7186def7ec72c', 'Pepe', '123987F'),
('970e4959470eec6ca8d5c8e3dd5d4521', 'yomogan', '123321A'),
('d86a609ac75bb2f9978b60a6756032fc', 'dani', '193455A'),
('f82616773ca784ed53654859870abdcb', 'dani', '123123F');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog`
--

CREATE TABLE `blog` (
  `id` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `text` varchar(200) NOT NULL,
  `cover` varchar(150) NOT NULL,
  `touched` int(11) NOT NULL,
  `IDblogger` varchar(100) NOT NULL,
  `blogger` varchar(50) NOT NULL,
  `data` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `blog`
--

INSERT INTO `blog` (`id`, `title`, `text`, `cover`, `touched`, `IDblogger`, `blogger`, `data`) VALUES
('033611cf14d2334836dd1bb8f26d95c9', 'Como duchar a tu perro', 'Para poder duchar a tu perro bien necesitas una ducha, jabÃƒÂ³n, agua y unas manos para enjabonar al perro.', '/workspace/miejer/FW_MVC_OO_JS_ANGULARJS/backend/media/perro-ducha.jpg', 147, '', 'Alejandro Pla Cambra', '2018-05-30 16:26:15'),
('d9f904fdc303218056831ab006c1e7a6', 'Peinar a tu perro', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '/workspace/miejer/FW_MVC_OO_JS_ANGULARJS/backend/media/portada_1.jpg', 54, 'blogger', 'Alejandro Pla Cambra', '2018-06-06 20:45:58'),
('db6d434cd9125740d8ccd51ef41982f6', 'Com estar to fort', 'pa estar to fort tens que anar en el aleja al gym, asi esta la foto de com alsa coses perque si', '/workspace/miejer/FW_MVC_OO_JS_ANGULARJS/backend/media/alejandro.jpg', 114, 'blogger', 'Alejandro Pla Cambra', '2018-06-04 16:07:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comments`
--

CREATE TABLE `comments` (
  `IDcomment` varchar(100) NOT NULL,
  `IDblog` varchar(100) NOT NULL,
  `text` varchar(50) NOT NULL,
  `IDuser` varchar(100) NOT NULL,
  `dateb` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comments`
--

INSERT INTO `comments` (`IDcomment`, `IDblog`, `text`, `IDuser`, `dateb`) VALUES
('11e8c5a8be7439b0e31a43b25b2b4b28', 'd9f904fdc303218056831ab006c1e7a6', 'asdasd', 'daniort', '2018-06-06 20:47:39'),
('264b7c004952b2ff0adf5f892ab72619', 'db6d434cd9125740d8ccd51ef41982f6', 'ixe tio es una torre', 'blogger', '2018-06-04 16:47:13'),
('30585ec1d73045620e5a997f7f206b08', 'db6d434cd9125740d8ccd51ef41982f6', 'ise tio esta to petat', 'blogger', '2018-06-04 16:24:05'),
('53e8376231194c61b5733e6a7828e216', 'db6d434cd9125740d8ccd51ef41982f6', 'Tens un 10 en cos pero un 4,99 en programacio', 'yomogan', '2018-06-04 16:53:39'),
('7993137c74dc8cfa1ee26c6cd2b4878f', 'd9f904fdc303218056831ab006c1e7a6', 'aaaaaaaaaaa', 'blogger', '2018-06-06 20:46:50'),
('7f91caafbb51f6b58de976462b7fcee8', 'db6d434cd9125740d8ccd51ef41982f6', 'Gracies yomogan', 'daniort', '2018-06-04 16:55:52'),
('c7a1ce25ccfcb0a46c289866dd46e19e', 'db6d434cd9125740d8ccd51ef41982f6', 'tu que faig jo ahi?', 'aleja', '2018-06-04 16:52:11'),
('cac05408847d72a16e74503d3040261e', 'db6d434cd9125740d8ccd51ef41982f6', 'ixe pilar el tinc jo al costat', 'daniort', '2018-06-04 16:51:35'),
('cf93ac20eca35da33827d370e80f70ee', 'db6d434cd9125740d8ccd51ef41982f6', 'que puta bestia', 'blogger', '2018-06-04 16:45:27'),
('e0edfae3c57eafbb6e150b02ebb8fe2b', 'db6d434cd9125740d8ccd51ef41982f6', 'Dani te un 10 en les 2 coses', 'yomogan', '2018-06-04 16:55:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dogs`
--

CREATE TABLE `dogs` (
  `name` varchar(50) NOT NULL,
  `chip` varchar(50) NOT NULL,
  `breed` varchar(50) NOT NULL,
  `tlp` int(11) NOT NULL,
  `date_birth` varchar(100) NOT NULL,
  `date_regis` varchar(100) NOT NULL,
  `cinfo` varchar(100) NOT NULL,
  `sex` varchar(50) NOT NULL,
  `stature` varchar(50) NOT NULL,
  `country` varchar(75) NOT NULL,
  `province` varchar(75) NOT NULL,
  `city` varchar(75) NOT NULL,
  `dinfo` varchar(200) NOT NULL,
  `picture` varchar(100) NOT NULL,
  `lat` varchar(100) NOT NULL,
  `longit` varchar(100) NOT NULL,
  `state` int(11) NOT NULL,
  `owner` varchar(50) NOT NULL,
  `fecha` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `dogs`
--

INSERT INTO `dogs` (`name`, `chip`, `breed`, `tlp`, `date_birth`, `date_regis`, `cinfo`, `sex`, `stature`, `country`, `province`, `city`, `dinfo`, `picture`, `lat`, `longit`, `state`, `owner`, `fecha`) VALUES
('Pipo', '123123F', 'Rottweiler', 123123123, '01/01/2018', '03/20/2018', 'Castrado,Vacunado', 'perra', 'mediana', 'Spain', 'Valencia', 'Ontinyent', 'Una perra muy carinosa y juguetona', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/rottweiler.jpg', '38.8246597', '-0.6038852', 1, 'oOlPbGBlGXVmJFLuC2Gjv2vLIr93', 'now()'),
('Lara', '123123G', 'Golden Retriever', 123123123, '01/01/2017', '03/21/2018', 'Castrado,,CariÃ±oso', 'perro', 'pequeÃ±a', 'Spain', 'Valencia', 'Ontinyent', 'Una perra que le gusta estar por el campo y pasear por la noche', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/golden-retriever.jpg', '38.8256696', '-0.5997988', 0, 'oOlPbGBlGXVmJFLuC2Gjv2vLIr93', '2018-03-19 20:21:50'),
('pepe', '123123K', 'Mestizo', 612312312, '01/02/2018', '', '', 'perro', 'pequeÃ±a', '', 'Ciudad Real', 'Almedina', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '/workspace/miejer/FW_MVC_OO_JS_ANGULARJS/backend/media/mestizo5.jpg', '', '', 2, 'YerBwm1SxtNCPvew4XlomKmYWEb2', '2018-05-29 18:34:27'),
('Fluffy', '123123Q', 'Akita', 123123132, '01/01/2018', '03/20/2018', 'CariÃ±oso,Traquilo,Jugueton', 'perra', 'grande', 'Spain', 'Valencia', 'Ontinyent', 'Perro muy carinoso que siempre le gusta jugar con la gente de sus alrededores', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/akita.jpg', '38.8236059', '-0.5989085', 0, 'oOlPbGBlGXVmJFLuC2Gjv2vLIr93', '2018-03-19 20:56:37'),
('Marco', '123123U', 'Pug', 123123123, '01/04/2018', '03/23/2018', 'Castrado,Jugueton', 'perro', 'pequeÃ±a', 'Spain', 'Valencia', 'Ontinyent', 'Perro vago que le gusta estar todos los dias sentado en su cama', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/pug.jpg', '38.8197356', '-0.601364', 2, 'dani', '2018-03-19 20:49:23'),
('Xaic', '123321A', 'Pastor Aleman', 123123123, '12/19/2014', '03/22/2018', 'Castrado,Jugueton', 'perro', 'pequeÃ±a', 'Spain', 'Valencia', 'Ontinyent', 'Es un gos molt carinyos y jugeto', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/pastor_aleman.jpg', '38.8190292', '-0.6037812', 0, 'YerBwm1SxtNCPvew4XlomKmYWEb2', '2018-03-20 16:53:24'),
('Lolo', '123654B', 'Pug', 123123123, '01/01/2018', '04/02/2018', 'Castrado,Jugueton', 'perro', 'pequeÃ±a', 'Spain', 'Valencia', 'Ontinyent', 'Perro que se pasa todo el dia jugando con su pelota preferida', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/pug3.jpg', '38.8174394', '-0.6104931', 0, 'YerBwm1SxtNCPvew4XlomKmYWEb2', '2018-04-02 19:08:31'),
('Beethoven', '123987F', 'San Bernardo', 123123123, '02/08/2017', '03/23/2018', 'Traquilo,Jugueton', 'perra', 'grande', 'Spain', 'Valencia', 'Albaida', 'Perra muy vaga que se pasa todo el dia acostada', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/San-bernardo.jpg', '38.842772', '-0.521731', 1, 'dani', '2018-03-23 10:23:12'),
('Burmi', '164323H', 'Teckel', 652347823, '01/30/2018', '04/17/2018', 'Castrado,CariÃ±oso,Traquilo,Jugueton', 'perra', 'pequeÃ±a', 'Spain', 'Alava', 'Abetxuko', 'Perro bipolar muy tranquilo y jugueton a la vez', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/teckel.jpg', '42.8762929', '-2.681201', 0, 'daniort', '2018-04-17 10:33:36'),
('Mostaza', '193455A', 'Golden Retriever', 123123123, '01/11/2018', '03/23/2018', 'CariÃ±oso,Jugueton', 'perra', 'pequeÃ±a', 'Spain', 'Valencia', 'Aielo De Malferit', 'Le gusta ir a correr al campo', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/golden-retriever2.jpg', '38.8773553', '-0.5908794', 1, 'dani', '2018-03-23 10:10:00'),
('Golfo', '263234R', 'Labrador', 123123123, '02/02/2016', '03/23/2018', 'CariÃ±oso,Traquilo', 'perro', 'grande', 'Spain', 'Valencia', 'Ontinyent', 'Perro muy olgazan que no da ni un paso', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/labrador.jpg', '38.808147', '-0.6029789', 0, 'YerBwm1SxtNCPvew4XlomKmYWEb2', '2018-03-23 10:27:52'),
('Lusi', '323475B', 'Mestizo', 123123123, '01/01/2018', '04/04/2018', 'Castrado,Vacunado', 'perra', 'mediana', 'Spain', 'Valencia', 'Aielo De Malferit', 'Le gusta mucho jugar con otros perros', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/default_avatar.svg', '38.8813618', '-0.5948583', 0, 'dani', '2018-04-04 12:37:21'),
('Tommy', '513454A', 'Labrador', 123123123, '02/01/2018', '03/23/2018', 'Castrado,Vacunado', 'perro', 'pequeÃ±a', 'Spain', 'Valencia', 'Ontinyent', 'Perro muy simpatico y carinoso', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/labrador2.jpg', '38.8059195', '-0.6039683', 0, 'daniort', '2018-03-23 10:42:45'),
('Chufa', '521123G', 'Golden Retriever', 123123123, '01/31/2018', '03/23/2018', 'Traquilo,Jugueton', 'perra', 'pequeÃ±a', 'Spain', 'Valencia', 'Aielo De Malferit', 'Le gusta jugar con otros perros', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/Golden-Retriever3.jpg', '38.8769496', '-0.5921691', 1, 'dani', '2018-03-23 10:12:46'),
('Negri', '524243F', 'Mestizo', 666666660, '05/07/2014', '', '', 'perra', 'grande', '', 'Valencia', 'Bocairent', 'negri es extrovertida y a veces ladra', '/workspace/miejer/FW_MVC_OO_JS_ANGULARJS/backend/media/negri.jpg', '', '', 0, 'dani', '2018-05-30 21:34:32'),
('Lara', '542312F', 'Golden Retriever', 123123123, '11/15/2017', '03/23/2018', 'Castrado,Vacunado', 'perra', 'mediana', 'Spain', 'Valencia', 'Aielo De Malferit', 'Perra que le gusta jugar con otros perros', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/golden-retriever4.jpg', '38.879303', '-0.5867681', 1, 'dani', '2018-03-23 10:45:28'),
('Rex', '623465G', 'Pastor Aleman', 123123123, '08/01/2017', '03/23/2018', 'CariÃ±oso,Traquilo', 'perro', 'grande', 'Spain', 'Valencia', 'Ontinyent', 'Perro muy tranquilo le gusta mucho estar al sol', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/pastor_aleman2.jpg', '38.825241', '-0.6136515', 0, 'daniort', '2018-03-22 19:24:55'),
('Oliv', '631212V', 'Shiba Inu', 123123123, '07/04/2017', '03/23/2018', 'Castrado,Vacunado', 'perra', 'mediana', 'Spain', 'Valencia', 'Agullent', 'Perra que le gusta estar siempre cerca de alguna persona', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/shibainu2.jpg', '38.8227834', '-0.5498438', 0, 'daniort', '2018-03-23 10:26:33'),
('Leo', '634534L', 'Caniche', 123653453, '04/12/2017', '04/17/2018', 'Vacunado,CariÃ±oso', 'perro', 'mediana', 'Spain', 'Alicante', 'Agres', 'perro muy pero que muy carinoso', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/caniche1.jpg', '38.7801135', '-0.5157832', 0, 'daniort', '2018-04-17 11:10:24'),
('Rosky', '652434Z', 'Beagle', 123123123, '01/04/2018', '03/23/2018', 'Traquilo,Jugueton', 'perro', 'pequeÃ±a', 'Spain', 'Valencia', 'Ontinyent', 'Es un cachorro que le gusta jugar con las pelotas', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/Beagle.jpg', '38.8221537', '-0.6158103', 0, 'aleja', '2018-03-23 10:29:39'),
('Siba', '734123L', 'Shiba inu', 123123123, '03/30/2017', '03/23/2018', 'Vacunado,CariÃ±oso', 'perro', 'mediana', 'Spain', 'Valencia', 'Ontinyent', 'Perro muy deportista necesita salir todos los dias la parque', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/shibainu.jpg', '38.8278086', '-0.6296346', 0, 'aleja', '2018-03-23 10:25:07'),
('Thor', '734523H', 'Pastor Belga', 123123123, '03/16/2017', '03/23/2018', 'Vacunado,Traquilo', 'perro', 'pequeÃ±a', 'Spain', 'Valencia', 'Albaida', 'Perro muy rapido que le gusta el deporte', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/pastor-belga.jpg', '38.8427179', '-0.5234896', 0, 'aleja', '2018-03-23 10:21:05'),
('Alonso', '735234S', 'Pug', 123123123, '11/01/2017', '03/23/2018', 'Castrado,Vacunado', 'perro', 'pequeÃ±a', 'Spain', 'Valencia', 'Ontinyent', 'Perro muy gracioso y divertido', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/pug2.jpg', '38.818699', '-0.6065192', 1, 'aleja', '2018-03-23 10:19:36'),
('Backy', '736123E', 'Rottweiler', 123123123, '02/06/2018', '03/23/2018', 'Castrado,Vacunado', 'perra', 'pequeÃ±a', 'Spain', 'Valencia', 'Ontinyent', 'Le gusta mucho jugar con la pelota', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/rottweiler3.jpg', '38.8192339', '-0.611151', 1, 'dani', '2018-03-23 10:07:06'),
('Beni', '742243V', 'Alaska Malamute', 123123123, '03/23/2017', '03/24/2018', 'Castrado,Jugueton', 'perro', 'mediana', 'Spain', 'Valencia', 'Ontinyent', 'Perro que le gusta estar tranquilo', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/alaskan-malamute.jpg', '38.82185', '-0.6092405', 0, 'yomogan', '2018-03-24 12:35:42'),
('Luky', '865234G', 'Boxer', 612375441, '11/14/2017', '04/17/2018', 'Castrado,Vacunado,Jugueton', 'perro', 'mediana', 'Spain', 'Valencia', 'Valencia', 'Perro que no puede estar quieto', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/boxer.jpg', '39.4673502', '-0.3823296', 0, 'dani', '2018-04-17 12:43:02'),
('Golfo', '917345B', 'Labrador', 612312634, '01/24/2018', '', '', 'perro', 'mediana', '', 'Valencia', 'Albuixech', '', '/workspace/miejer/FW_MVC_OO_JS_ANGULARJS/backend/media/labrador3.jpg', '', '', 1, 'Pepe', '2018-05-29 11:13:18'),
('Tobi', '976345F', 'Rottweiler', 123123123, '02/03/2016', '03/23/2018', 'CariÃ±oso,Traquilo', 'perro', 'mediana', 'Spain', 'Valencia', 'Agullent', 'Perro muy tranquilo y carinoso', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/Rottweiler_2.jpg', '38.8230534', '-0.5545122', 1, 'daniort', '2018-03-23 10:03:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rate`
--

CREATE TABLE `rate` (
  `IDrate` varchar(150) NOT NULL,
  `IDblog` varchar(150) NOT NULL,
  `IDuser` varchar(150) NOT NULL,
  `titration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rate`
--

INSERT INTO `rate` (`IDrate`, `IDblog`, `IDuser`, `titration`) VALUES
('6d2d1976ba466bfbf03ee5e7843e0655', '033611cf14d2334836dd1bb8f26d95c9', 'blogger', 5),
('c0916fbb5e7f80a1dcded725b4c41887', '033611cf14d2334836dd1bb8f26d95c9', 'daniort', 5),
('e013b0dc32319d612c8b854cb9f04a0c', 'db6d434cd9125740d8ccd51ef41982f6', 'blogger', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `IDuser` varchar(100) NOT NULL,
  `user` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `type` varchar(45) NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `activate` tinyint(1) NOT NULL DEFAULT '0',
  `token` varchar(100) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `birthday` varchar(100) NOT NULL,
  `tokenlog` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`IDuser`, `user`, `email`, `password`, `type`, `avatar`, `activate`, `token`, `name`, `surname`, `birthday`, `tokenlog`) VALUES
('aleja', 'aleja', 'danipuerta54@gmail.com', '$1$rasmusle$GwKwU6oOyiZo0t0sa5t7R/', 'user', 'https://www.gravatar.com/avatar/24261b4c198a7c26da49098b73f52956?s=80&d=identicon&r=g', 1, '97b0ae7ebfcfe124b995bf9252112395', '', '', '', 'b90971a989368b37deca2e5599af96b0'),
('Ard9a1tR52WOoi434QdpeDQl12q2', 'Ard9a1tR52WOoi434QdpeDQl12q2', '', '', 'user', 'https://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png', 1, '4edc3012dcf873b39400053712eff4ce', '', '', '', ''),
('blogger', 'blogger', 'danipuerta54@gmail.com', '$1$rasmusle$GwKwU6oOyiZo0t0sa5t7R/', 'blogger', '/workspace/miejer/FW_MVC_OO_JS_ANGULARJS/backend/media/alejandro.jpg', 1, 'a114010110aa043ec286d6014c3c2a05', 'Alejandr', 'Pla Cambra', '12/19/1998', 'cd3648cbac5ae23505f7735259eadf94'),
('dani', 'dani', 'danipuerta54@gmail.com', '$1$rasmusle$3cgeBswGjY2s3H/COKc0B0', 'admin', '/workspace/miejer/FW_MVC_OO_JS_ANGULARJS/backend/media/peril.png', 1, '9363b08238d4360b12e5c77fc2700d34', 'Daniel', 'Ortiz Garcia', '03/04/2002', 'eb99ca0cda8cf628ae4025b81da22eed'),
('daniort', 'daniort', 'danipuerta54@gmail.com', '$1$rasmusle$GwKwU6oOyiZo0t0sa5t7R/', 'user', '/workspace/miejer/FW_MVC_OO_JS_ANGULARJS/backend/media/perfil5.png', 1, '16dfd4a4f5b73fc0ae3d02508bc7f9af', 'Dani', 'Ortiz Garcia', '12/11/1998', 'ab52e0e362bd9e322da6b96aeb0dae63'),
('gGTrh2mugaQkKLkDlazViVCT0872', 'gGTrh2mugaQkKLkDlazViVCT0872', '', '', 'user', 'https://pbs.twimg.com/profile_images/378800000006918826/330d65fb000f232068c4987e1eb690a0_normal.jpeg', 1, 'dc53762cfaf628bee475adfcee5af25f', '', '', '', ''),
('oOlPbGBlGXVmJFLuC2Gjv2vLIr93', 'oOlPbGBlGXVmJFLuC2Gjv2vLIr93', 'danipuerta54@gmail.com', '', 'user', 'https://lh6.googleusercontent.com/-eWUex2a93VY/AAAAAAAAAAI/AAAAAAAAAZE/fhwLTLTngjs/photo.jpg', 1, '942fe302009ac45096de5b676f9fedc5', '', '', '', 'bfd2998803e719a1b705fd864fb4d1f0'),
('Pepe', 'Pepe', 'danipuerta54@gmail.com', '$1$rasmusle$3cgeBswGjY2s3H/COKc0B0', 'user', '/workspace/miejer/FW_MVC_OO_JS_ANGULARJS/backend/media/peril.png', 1, '17211fb40f6dff0ab3e1a8a3812f26c6', 'Dani', 'Ortiz', '12/11/1998', 'c8b6cd42077a91d513a1e4af5b4ce6c6'),
('YerBwm1SxtNCPvew4XlomKmYWEb2', 'YerBwm1SxtNCPvew4XlomKmYWEb2', 'daniortizgar@gmail.com', '', 'user', '/workspace/miejer/FW_MVC_OO_JS_ANGULARJS/backend/media/perfil5.png', 1, '', 'Dani', 'Ortiz', '05/05/1999', '609e4bdee96e303cace33ba7dfe99c73'),
('yomogan', 'yomogan', 'danipuerta54@gmail.com', '$1$rasmusle$GwKwU6oOyiZo0t0sa5t7R/', 'user', '/workspace/miejer/FW_MVC_OO_JS_JQUERY/media/perfil.jpeg', 1, '048501987c3a6b42d033347738c27d8c', 'yolanda', 'monerris', '10/10/2000', 'e45cf69b3f8c70cad676871d38d02ab0');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `adoption`
--
ALTER TABLE `adoption`
  ADD PRIMARY KEY (`id_token`);

--
-- Indices de la tabla `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`IDcomment`);

--
-- Indices de la tabla `dogs`
--
ALTER TABLE `dogs`
  ADD PRIMARY KEY (`chip`);

--
-- Indices de la tabla `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`IDrate`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`IDuser`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
