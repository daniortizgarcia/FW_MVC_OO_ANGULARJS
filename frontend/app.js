var ohanadogs = angular.module('ohanadogs', ['ngRoute', 'toastr', 'ui.bootstrap','ngMdIcons']);
ohanadogs.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
                // Home
                .when("/", {templateUrl: "frontend/modules/home/view/home.view.html", controller: "mainCtrl",
                    resolve: {
                        names: function (services) {
                            return services.get('home','load_name');
                        },
                        bbreeds: function (services) {
                            return services.get('home','best_breed');
                        }
                    }
                })

                .when("/home/:id", {
                    templateUrl: "frontend/modules/home/view/details.view.html",
                    controller: "detailsBCtrl",
                    resolve: {
                        selbreed: function (services, $route) {
                            return services.get('home', 'load_list', $route.current.params.id);
                        }
                    }
                })

                .when("/home/active_user/:token", {
                    resolve: {
                        recpass: function (services, $route) {
                            return services.put('home','active_user',{'token':JSON.stringify({'token':$route.current.params.token})}).then(function(){
                                location.href = '#/';
                            });
                        }
                    }
                })

                // Contact
                .when("/contact", {templateUrl: "frontend/modules/contact/view/contact.view.html", controller: "contactCtrl"})

                // Adoptions
                .when("/adoptions", {
                    templateUrl: "frontend/modules/adoptions/view/adoptions.view.html", 
                    controller: "adoptionsCtrl",
                    resolve: {
                        adoptions: function (services) {
                            return services.get('adoptions', 'load_list','%');
                        },
                        breeds: function (services) {
                            return services.get('adoptions', 'all_breeds');
                        }
                    }
                })

                // Ubication
                .when("/ubication", {
                    templateUrl: "frontend/modules/ubication/view/ubication.view.html", 
                    controller: "ubicationCtrl",
                    resolve: {
                        ubications: function (services) {
                            return services.get('ubication', 'load_location');
                        },
                        provinces: function (services) {
                            return services.get('ubication', 'load_prov');
                        }
                    }
                })

                // Dogs
                .when("/dogs", {
                    templateUrl: "frontend/modules/dogs/view/dogs.view.html", 
                    controller: "dogsCtrl",
                    resolve: {
                        provinces: function (services) {
                            return services.get('dogs', 'load_provinces');
                        }
                    }
                })

                //Blogger
                .when("/blogger", {
                    templateUrl: "frontend/modules/blog/view/create_blog.view.html", controller: "bloggerCtrl"
                })

                //Blog
                .when("/blog", {
                    templateUrl: "frontend/modules/blog/view/view_blog.view.html", 
                    controller: "blogCtrl",
                    resolve: {
                        blogs: function (services) {
                            return services.get('blog', 'load_blogs');
                        },
                        comments: function (services) {
                            return services.get('blog', 'load_comments');
                        },
                        rate: function(services){
                            return services.get('blog', 'load_rate');
                        }
                    }
                })

                //Login
                .when("/login", {
                    templateUrl: "frontend/modules/login/view/login.view.html",controller: "loginCtrl"})

                //ChangePass
                .when("/login/changepass/:token", {
                    templateUrl: "frontend/modules/login/view/recpass.view.html",
                    controller: "changepassCtrl"
                })

                //Profile
                .when("/profile", {
                    templateUrl: "frontend/modules/login/view/profile.view.html",
                    controller: "profileCtrl",
                    resolve: {
                        infoUser: function (services,localstorageService) {
                            return services.get('login', 'print_user',localstorageService.getUsers());
                        },
                        infoVal: function (services,localstorageService) {
                            return services.get('login', 'print_val',localstorageService.getUsers());
                        }
                    }
                })

                // else 404
                .otherwise("/", {templateUrl: "frontend/modules/home/view/home.view.html", controller: "mainCtrl"});
    }]);
