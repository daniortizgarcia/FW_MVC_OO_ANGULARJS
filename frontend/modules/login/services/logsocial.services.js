ohanadogs.factory("socialService", ['$rootScope', 'services','localstorageService','toastr','$timeout',
function ($rootScope, services, localstorageService, toastr, $timeout) {
	var service = {};
	service.initialize = initialize;
	service.insertData = insertData;
    return service;

    function initialize() {
        var config = {
            apiKey: "AIzaSyB1U9yIlJb9sPq_UbgqJtxon5tEXQCaNTY",
            authDomain: "ohanafirebase.firebaseapp.com",
            databaseURL: "https://ohanafirebase.firebaseio.com",
            projectId: "ohanafirebase",
            storageBucket: "ohanafirebase.appspot.com",
            messagingSenderId: "751508086282"
        };
        firebase.initializeApp(config);
    };

    function insertData(user,name,email,avatar){
        var sname = name.split(' ');
        var name = sname[0];
        var surname = sname[1];
    	services.post('login','log_social',{'data_social_net':JSON.stringify({'id_user':user,'user':user,'name':name,'surname':surname,email:email,'avatar':avatar})}).then(function(response){
    		localstorageService.setUsers(JSON.parse(response));
    		toastr.success('Inicio de sesion correcto', 'Perfecto',{
                closeButton: true
            });
            $timeout( function(){
	            location.href = '.';
	        }, 3000 );
    	});
    }
}]);

ohanadogs.factory("facebookService", ['$rootScope', 'services','socialService',
function ($rootScope, services, socialService) {
	var service = {};
	service.login = login;
    return service;

    function login() {
    	var provider = new firebase.auth.FacebookAuthProvider();

        var authService = firebase.auth();

        authService.signInWithPopup(provider).then(function(result) {
            socialService.insertData(result.user.uid,result.user.displayName,result.user.email,result.user.photoURL);
        })
        .catch(function(error) {
            console.log('Se ha encontrado un error:', error);
        });
    };

}]);

ohanadogs.factory("googleService", ['$rootScope', 'services','socialService',
function ($rootScope, services,socialService) {
	var service = {};
	service.login = login;
    return service;

    function login() {
    	var provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope('email');
    
        var authService = firebase.auth();

        authService.signInWithPopup(provider).then(function(result) {
            socialService.insertData(result.user.uid,result.user.displayName,result.user.email,result.user.photoURL);
        })
        .catch(function(error) {
            console.log('Se ha encontrado un error:', error);
        });
    };

}]);

ohanadogs.factory("twitterService", ['$rootScope', 'services','socialService',
function ($rootScope, services, socialService) {
	var service = {};
	service.login = login;
    return service;

    function login() {
    	var provider = new firebase.auth.TwitterAuthProvider();
      	var authService = firebase.auth();
  
      	authService.signInWithPopup(provider).then(function(result) {
          	socialService.insertData(result.user.uid,result.user.displayName,result.user.email,result.user.photoURL);
	    }).catch(function(error) {
	      	console.log('Se ha encontrado un error:', error);
	    });
    };

}]);
