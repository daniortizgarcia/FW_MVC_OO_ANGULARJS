ohanadogs.controller('bloggerCtrl', function($scope,services,localstorageService,toastr,$timeout) {
	

	$scope.dropzoneConfig = {
        'options': {
            'url': 'backend/index.php?module=blog&function=upload_cover',
            addRemoveLinks: true,
            maxFileSize: 1000,
            dictResponseError: "Ha ocurrido un error en el server",
            acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd'
        },
        'eventHandlers': {
            'sending': function (file, formData, xhr) {},
            'success': function (file, response) {
            	console.log(response)
                response = JSON.parse(response);
                if (response.result) {
                    $(".msg").addClass('msg_ok').removeClass('msg_error').text('Success Upload image!!');
                    $('.msg').animate({'right': '300px'}, 300);
                } else {
                    $(".msg").addClass('msg_error').removeClass('msg_ok').text(response['error']);
                    $('.msg').animate({'right': '300px'}, 300);
                }
            },
            'removedfile': function (file, serverFileName) {
                if (file.xhr.response) {
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    var data = jQuery.parseJSON(file.xhr.response);
                    services.post("blog", "delete_cover", JSON.stringify({'filename': data.data}));
                }
            }
    }};

    $scope.submitBlog = function(argument) {
    	token = localstorageService.getUsers();
    	services.post("blog", "insert_blog", {'data_blog':JSON.stringify({'title': $scope.blog.inputTitle,'text':$scope.blog.inputText,'token':token})}).then(function(response) {
    		toastr.success('Blog creado correctamente', 'Perfecto',{
                closeButton: true
            });
            $timeout( function(){
            	location.href = '.';
            }, 3000 );
    	});
    }
});

ohanadogs.controller('blogCtrl', function($scope,services,blogs,comments,localstorageService,toastr,$timeout,$uibModal,$route,rate) {
    $scope.disSelect = true;
    $scope.valVal = true;
    $scope.selectValB = false;
	array = [];
    array2 = [];
	$scope.dataBlog = blogs;
	$scope.contBlogV = true;
    $scope.showProv = false;
	$scope.sarBlog = function (idblog) {
		services.put('blog','stouch',{'id':idblog});
		blogs.forEach(function(data){
            if(data.id.toLowerCase().indexOf(idblog) !== -1){
            	$scope.detBlog = data;
            }
        });
        comments.forEach(function(data){
            if(data.IDblog.toLowerCase().indexOf(idblog) !== -1){
            	array.push(data)
            }
        });
        rate.forEach(function(data){
            if(data.IDblog.toLowerCase().indexOf(idblog) !== -1){
                array2.push(data)
            }
        });
        $scope.dataComments = array;
        $scope.dataRate = array2[0];
        console.log($scope.dataRate)
        if (array.length < 1) {
        	$scope.valComm = true;
        }else{
        	$scope.valComm = false;
        }
        if (array2.length < 1) {
            $scope.valVal = false;
        }else{
            $scope.valVal = true;
        }

		$scope.detBlogV = true;
		$scope.contBlogV = false;
		array = [];
        array2 = [];
	}
	$scope.submitComment = function(idblog){
		token = localstorageService.getUsers();
		services.post("blog", "insert_comment", {'data_comment':JSON.stringify({'text':$scope.comment.inputText,'idblog':idblog,'token':token})}).then(function(response) {
			console.log(response)
    		toastr.success('comentario creado correctamente', 'Perfecto',{
                closeButton: true
            });
            $timeout( function(){
            	location.href = '.';
            }, 2000 );
    	});
	}
	$scope.openProf = function(id){
		var modalInstance = $uibModal.open({
	        animation: 'true',
	        templateUrl: 'frontend/modules/blog/view/modProf.view.html',
	        controller: 'modProfCtrl',
	        windowClass : 'show',
	        size: "lg",
	        resolve: {
                   infoUser: function () {
                        return services.get('blog','get_user',id);
                    }
                }
	    });
	}

    $scope.insertVal = function (id){
        $scope.disSelect = false;
        token = localstorageService.getUsers();
        if ($scope.selectVal > 0) {
            services.put('blog', 'insert_val',{'infoVal':JSON.stringify({'numb':$scope.selectVal,'idblog':id,'token':token})}).then(function (response) {
                if (response != 'false') {
                    toastr.success('Valoracion creada correctamente', 'Perfecto',{
                    closeButton: true
                    });
                    $timeout( function(){
                        $route.reload();
                    }, 2000 );
                }else{
                    toastr.error('Error al valorar el blog', 'Error',{
                        closeButton: true
                    });
                    $scope.disSelect = true;
                }
            });
        }
        
    }

	$scope.butVolver = function(){
		$scope.detBlogV = false;
		$scope.contBlogV = true;
	}
});

ohanadogs.controller('modProfCtrl', function($scope,$uibModalInstance,infoUser) {
	console.log(infoUser)
	$scope.infoUser = infoUser;
	$scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
});