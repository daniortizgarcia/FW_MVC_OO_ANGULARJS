ohanadogs.controller('contactCtrl', function($scope,services,toastr){
	$scope.contact = {
        inputName: "",
        inputEmail: "",
        inputSubject: "",
        inputMessage: ""
    };
    $scope.SubmitContact = function () {
        var data = {"name": $scope.contact.inputName, "email": $scope.contact.inputEmail, 
        "matter": $scope.contact.inputSubject, "message": $scope.contact.inputMessage,"token":'contact_form'};
        var contact_form = JSON.stringify(data);
        console.log(contact_form);
        services.post('contact', 'send_cont', contact_form).then(function (response) {
            if (response == 'true') {
                toastr.success('El mensaje ha sido enviado correctamente', 'Mensaje enviado',{
                    closeButton: true
                });
            } else {
                toastr.error('El mensaje no se ha enviado', 'Mensaje no enviado',{
                    closeButton: true
                });
            }
        });
    };
});